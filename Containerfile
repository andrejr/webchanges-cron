FROM ubuntu:jammy
LABEL maintainer="Andrej Radović (r.andrej@gmail.com)"

ENV CRON_TIME='*/30 * * * *'

RUN set -xe \
    && apt-get update  \
    && DEBIAN_FRONTEND=noninteractive TZ=Etc/UTC apt-get -y install \
    build-essential \
    cron \
    bash \
    wget \
    python3 \
    python3-pip \
    ca-certificates \
    libffi-dev \
    libffi8 \
    libxml2          \
    libxml2-dev      \
    tzdata \
    && find /var/lib/apt -type f -delete \
    && find /var/cache/apt -type f -delete \
    && pip3 install --no-cache-dir \
    aioxmpp        \
    appdirs        \
    beautifulsoup4 \
    chump          \
    cssbeautifier  \
    cssselect      \
    html2text      \
    jq             \
    jsbeautifier   \
    keyrings.alt   \
    lxml           \
    markdown2      \
    matrix_client  \
    minidb         \
    pushbullet.py  \
    pyyaml         \
    requests       \
    webchanges[use_browser]


COPY crond_start /usr/bin

VOLUME /root/.config/webchanges
WORKDIR /root/.config/webchanges

CMD ["/usr/bin/crond_start"]
