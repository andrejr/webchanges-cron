# webchanges-cron

A simple containerization of 
[webchanges](https://github.com/mborsetti/webchanges), scheduled with `cron`.

## Usage

Here's an example:
```
podman run -it --rm \
    -e CRON_TIME="*/15 * * * *" \
    -v your_webchanges_folder:/root/.config/webchanges \
    quay.io/andrej/webchanges-cron:latest
```

You need to mount your webchanges folder (which contains `urls.yaml`, 
`webchanges.yaml`,…) to `/root/.webchanges`.

You specify the `cron` time expression with the `CRON_TIME` environment 
variable, (it's `*/30 * * * *` by default - *every 30 minutes*).

The source code can be found [here](https://gitlab.com/andrejr/webchanges-cron).
